struct VS_INPUT
{
    float3 pos : POSITION;
    float4 color: COLOR;
};

struct VS_OUTPUT
{
    //SV_POSITION语义是一个系统语义，VS必须返回一个由SV_POSITION关联的position，供下面的管线阶段使用
    //比如光栅化阶段将会在多边形表面上对顶点数据进行插值
    float4 pos : SV_POSITION;
    float4 color : COLOR;
};

cbuffer ConstantBuffer : register(b0)
{
    float colorMultiplier;
}

VS_OUTPUT main(VS_INPUT input)
{
    VS_OUTPUT output;
    output.pos = float4(input.pos, 1.0f);
    output.color = input.color * colorMultiplier;
    return output;
}
